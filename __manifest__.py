# -*- coding: utf-8 -*-
{
    'name': "Inventory multi transfer",

    'summary': """
        proses internal transfer 3 gudang ke satu toko
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "asopkarawang@gamil.com",

    'category': 'Inventory',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','stock','article_product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/stock_move.xml',
        'views/stock_picking_batch.xml',
        'report/report_picking_list.xml',
    ],
}