from odoo import models, fields, api, _

class internal_stock_picking(models.Model):
	_inherit ='stock.picking'
	_order = 'origin'


class internal_stock_move(models.Model):
	_inherit ='stock.move'

	barcode_vendor = fields.Char('Barcode Vendor')
	article = fields.Text('Article')
	unit_price = fields.Integer('Harga')
	sub_total = fields.Integer('Jumlah')
	keterangan = fields.Text('Keterangan')
	no_packing = fields.Char('No Packing')



	@api.onchange('product_id','product_uom_qty')
	def onchange_article_barcode(self):
		product = self.product_id
		self.barcode_vendor = product.barcode_vendor
		self.article = product.article
		self.unit_price = product.lst_price
		self.sub_total = self.unit_price * self.product_uom_qty